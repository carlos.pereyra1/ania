###Tuplas###

mi_tupla = tuple()
mi_otra_tupla = ()

mi_tupla = [29, "Carlin", "Carlin", 1.70, "informática"]

print(mi_tupla)

print(mi_tupla[0])
print(mi_tupla[3])
print(mi_tupla[-2])

print("Carlin aparece",mi_tupla.count("Carlin"),"veces")

print(mi_tupla.index("Carlin")) #indica en que posicion en encuetra el valor
#reemplazando valores
mi_tupla[0] = 30
mi_tupla.insert(1, "Toma mate dulce")

print(mi_tupla) 

mi_otra_tupla = [60, 0, 31, 10]
print(mi_otra_tupla)

#Al sumar, se hace una lista
sumando_tuplas = mi_tupla + mi_otra_tupla 
print("La suma de las tuplas es:",sumando_tuplas)

#Solamente imprime valores indicados

print(sumando_tuplas[1:3])
#convirtiendo la tupla en lista en el caso de añadir funciones que se necesiten de las listas

mi_tupla = list(mi_tupla)
print(type(mi_tupla))

#convirtiendo de lista a tupla en el caso de no necesitar modificaciones
mi_tupla = tuple(mi_tupla)
print(type(mi_tupla))

#borrar tuplas no se puede
sumando_tuplas = tuple(sumando_tuplas)
#del sumando_tuplas[5] ERROR 
print(sumando_tuplas)
print(type(sumando_tuplas))

#borrar la lista si se puede
sumando_tuplas = list(sumando_tuplas) #cambio de tipo
del sumando_tuplas[5] #pos 5 informatica
print(sumando_tuplas)
print(type(sumando_tuplas))