#Imprimiendo caracter por caracter

languaje = "python"
languaje_may = "PYTHON"

a, b, c , d ,e ,f = languaje #se debe poner la cantidad exacta de caracteres q usa la frase, en este caso 6
print(a, b, c, d, e, f) #imprime uno al lado del otro
print(a)#imprimiendo uno abajo del otro ___
print(b)
print(c)
print(d)
print(e)
print(f)

languaje_slice= languaje[0:6] #imprime desde el caracter 0 al 6, al caracter que se le asigne, si se le pone solo un nro, va desde ese nro en adelante
print(languaje_slice)

#para imprimir alrevez
reverse_lang = languaje [::-1]
print(reverse_lang)

#funciones 
print(languaje.capitalize())
print(languaje.upper()) #mayuscula
print(languaje.count("t"))
print("1".isnumeric()) #true
print(languaje.isnumeric()) #false
print(languaje_may.lower()) #minuscula
print(languaje_may.lower().isupper()) #es minuscula pero pregunta si es mayuscula y da false
print(languaje.startswith("Ph")) #Consulta si esta iniciando con la variable con Ph y es false
print(languaje.startswith("py")) #Consulta si esta iniciando con la variable con py y es true

