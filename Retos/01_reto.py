### R E T O 01 ###
"""
Escribe una función que reciba dos palabras (String) y retorne
verdadero o falso (Bool) según sean o no anagramas.
Un Anagrama consiste en formar una palabra reordenando TODAS
las letras de otra palabra inicial.
NO hace falta comprobar que ambas palabras existan.
Dos palabras exactamente iguales no son anagrama.
"""

def anagrama(palabra_uno, palabra_dos):
    if palabra_uno.upper() == palabra_dos.upper():
        return False
        
    return sorted(palabra_uno.upper()) == sorted(palabra_dos.upper())
        #para que funcione el print debo usar el if
        # print("Son anagramas", sorted(palabra_uno.upper()), sorted(palabra_dos.upper()), "\n")
print(anagrama("Amor", "rOMA")) #si hago un print(anagrama("Amor", "Roma")) retorna verdadero o falso