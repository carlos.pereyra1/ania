### R E T O 00 ###
"""
Escribe un programa que muestre por consola (con un print) los
números de 1 a 100 (ambos incluidos y con un salto de línea entre
cada impresión), sustituyendo los siguientes:
- Múltiplos de 3 por la palabra "fizz".
- Múltiplos de 5 por la palabra "buzz".
- Múltiplos de 3 y de 5 a la vez por la palabra "fizzbuzz".
"""

def fizzbus():
    for indice in range(1, 101):
        if indice % 3 == 0 and indice % 5 == 0: #para hacer el multiplo del nro que necesito es con %
            print("fizzbus")
        else:
            print(indice)
def fizz():
    for indice in range(1, 101):
        if indice % 3 == 0: #para hacer el multiplo del nro que necesito es con %
            print("fizz")
        else:
            print(indice)
def bus():
    for indice in range(1, 101):
        if indice % 5 == 0: #para hacer el multiplo del nro que necesito es con %
            print("bus")
        else:
            print(indice)


fizz()
bus()
fizzbus()

"""
mi_lista = [i for i in range(101)]
#print(mi_lista) #imprime el rango desde 0 al nro que le asigne.
 
multip_de_tres = [i*3 for i in range(35)]
#print(multip_de_tres) #imprime el rango desde 0 al nro que le asigne.

multip_de_cinco = [i*5 for i in range(21)]
#print(multip_de_cinco) #imprime el rango desde 0 al nro que le asigne.

if mi_lista == multip_de_tres:
    mi_lista = "fizz"
    print(mi_lista)
else:
    print("no anduvo")
"""