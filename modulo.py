### Modulo ###

"""
AMBAS FORMAS SON VALIDAS:
from pruebas import responder #accede solamente a la funcion que mencione, en este caso responder del archivo pruebas, pero con una coma se pueden poner todas las funciones q necesite

responder("") #llamada a la funcion

/////
import pruebas  #accede a todas las funciones del archivo pruebas

pruebas.responder("")#llamada a la funcion
"""
import pruebas  #accede a todas las funciones del archivo pruebas poniendo el punto y luego la funcion

pruebas.responder("")#llamada a la funcion