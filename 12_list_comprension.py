### Comprension de listas ###

mi_lista_original = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(mi_lista_original)

mi_lista = [i for i in range(6)]
print(mi_lista) #imprime el rango desde 0 al nro que le asigne.

mi_rango = range(20) #realiza la impresion de una lista, pero en el rango asignado
print(list(mi_rango))

mi_lista = [i for i in range(5)]
print(mi_lista) #imprime el rango(cant veces) desde 0 al nro que le asigne.

mi_lista = [i * 2 for i in range(10)]
print(mi_lista) #imprime el rango que deseo sin escribir tanto, de dos en dos hasta 10 veces desde 0 al nro que le asigne.

def sum_cinco(numero):
    return numero + 5

mi_lista = [sum_cinco(i) for i in range(10)] #parte desde cinco
print(mi_lista) #imprime el rango que deseo sin escribir tanto, de dos en dos hasta 10 desde 0 al nro que le asigne.
