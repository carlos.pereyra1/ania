### Listas (como arreglos pero mas flexible)###

mi_lista = list()
mi_otra_lista = []

print(len(mi_lista))

mi_lista = [10, 20, 30]
print(mi_lista)
print(len(mi_lista))

mi_otra_lista = [29, "carlin" , 1.70]
print(mi_otra_lista)
print(len(mi_otra_lista)) #cantidad de caracteres
print(type(mi_otra_lista))#el tipo de la variable que es de tipo list

#para acceder a las distintas posiciones de la lista

print(mi_otra_lista[0])
print(mi_otra_lista[1])
print(mi_otra_lista[2])
print(mi_otra_lista[-2]) #Resta los espacios de la lista, y llega al nro restado en este caso 0 - 2 = -2(espacios), no se puede exceder el nro de espacios de la lista
#print(mi_otra_lista[-6]) IndexError porque excede el nro de espacios de la lista
#print(mi_otra_lista[-4]) IndexError porque excede el nro de espacios de la lista

edad, nombre, medida = mi_otra_lista #para poder acceder a los elementos de la lista con una variable determinada.

print("voy a imprimir la medida:",medida)
print("voy a imprimir la edad:",edad)
print("voy a imprimir el nombre:",nombre)

print(mi_otra_lista + mi_lista) #concatenacion de listas

#Para insertar un elemento mas a la lista

mi_otra_lista.append("Hincha de Boca")

print(mi_otra_lista) #se agrego al final el campo hincha de boca a mi_otra_lista

mi_otra_lista.insert(1, "Toma mate dulce")
print(mi_otra_lista) #se agrego en la POS 1, el campo toma mate dulce a mi_otra_lista, no sobreescribe, sino que inserta lo que pedimos, y corre a otras posiciones los otros valores

mi_otra_lista.remove("Toma mate dulce")
print(mi_otra_lista) #se elimina el campo toma mate dulce a mi_otra_lista

#pop elimina el ultimo elemento de la lista
print(mi_otra_lista)
print(mi_otra_lista.pop()) #muestra que es lo que elimina

print(mi_otra_lista)
mi_elemento_pop = mi_otra_lista.pop(1) #elimina el elemento de la lista indicado, en este caso el de la pos 1

print(mi_elemento_pop)
print(mi_otra_lista)

del mi_otra_lista[1]#otra forma de eliminar un elemento es con DEL
edad= mi_otra_lista #para poder acceder a los elementos de la lista con una variable determinada.

#OJO, dependiendo donde se llame a mi_otra_lista, se va a obtener el valor actualizado

print(mi_otra_lista) #de esta manera la lista queda solamente ocn un campo, el 0 (edad)
print(edad) #imprime el unico valor que tiene

#remove elimina un elemento que conocemos, es decir el valor de la lista, mientras que del elimina por posicion indicada

#realizando copia de lista

print("mi lista tenia", mi_lista)
mi_nueva_lista = mi_lista.copy() #realiza la copia de mi lista y guarda en otra variable
mi_lista.clear() #limpia la lista

print("ahora mi_lista esta vacía", mi_lista)
print("la nueva lista tiene", mi_nueva_lista)

#para darle la vuelta e imprimir al reves
print(mi_nueva_lista)
mi_nueva_lista.reverse()
print(mi_nueva_lista)

#para ordenar la lista se usa sort()
mi_nueva_lista.sort()
print(mi_nueva_lista)