from fastapi  import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return "hola fastAPI!!"

@app.get("/url")#cambiar la direccion para que devuelva un resultado distinto
async def url():
    return { "URL" : "https://youtube.com"} 


#uvicorn main:app --reload para hacer correr el programa
#documentacion con Swagger: /docs
#documentacion con Redocly: /redoc