import pyttsx3


engine = pyttsx3.init()
#se puede o cargar un valor, o hacer que diga algo

engine.say("Hola, mi nombre es ANIA, que queres que diga?")
engine.runAndWait()
#hacer que diga algo
txt = input() 
engine.say(txt)
engine.runAndWait()

# guarda un archivo mp3
#engine.save_to_file("Hola, mi nombre es ANIA, que queres que diga?","test.mp3")

"""
PARA CAMBIAR A INGLÉS EL LENGUAJE
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[1].id)

guarda lo que escribi

engine.save_to_file(txt, "test2.mp3")

speaker = pyttsx3.init()
voices = speaker.getProperty('voices')
print(voices)

#changing index, changes voices, 0 for male
speaker.setProperty('voice', voices[0].id)

#changing index, changes voices, 1 for female
speaker.setProperty('voice', voices[1].id)
"""