### Date (fechas) ###

from datetime import datetime
from datetime import date
fecha_actual = date.today()
now = datetime.now()

def fecha_hora(ahora):
    if ahora == datetime.now():
        print("->imprimiendo fecha ACTUAL...\n")
        print("Nos encontramos en el año",ahora.year)
        print("La fecha actual es", ahora.day,"/",ahora.month, "/",ahora.year)
        print("La hora actual es",ahora.hour,":", ahora.minute,":", ahora.second)    
        print("El valor de timestamp:", ahora.timestamp(),"\n")

    else:
        print("->imprimiendo MI fecha...\n")
        print("Nos encontramos en el año",ahora.year)
        print("La fecha actual es", ahora.day,"/",ahora.month, "/",ahora.year)
        print("La hora actual es",ahora.hour,":", ahora.minute,":", ahora.second)    
        print("El valor de timestamp:", ahora.timestamp())

"""
Utilizando el timestamp()
para formato de computadora, a nivel lenguaje
se puede controlar mejor una fecha, a diferencia del formato anterior
"""
#MISMA FUNCION
fecha_hora(now)  #llamando func, se pasa el parametro requerido
#que imprima la fecha que quiero yo
mi_fecha = datetime(2022, 1, 1, 3, 10, 20) #año, mes, día, hora, min, seg
fecha_hora(mi_fecha) 
#--FIN--
#se puede usear datetime(), time(), date()
def fun_fecha_actual(actual):
    print("->ACTUAL...\n",actual)

fun_fecha_actual(fecha_actual)

#TIMEDELTA() para que guarde el valor de franjas de tiempo

from datetime import timedelta

inicio_timedelta = timedelta(200, 100, 100, weeks = 10)#valores aleatorios
fin_timedelta = timedelta(300, 100, 100, weeks = 13)#valores aleatorios

print("inicio",inicio_timedelta)
print("fin",fin_timedelta)

print("resta inicio y fin", fin_timedelta - inicio_timedelta)
print("suma inicio y fin", fin_timedelta + inicio_timedelta)


