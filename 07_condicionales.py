### Condicionales ###

mi_condicion = True

if mi_condicion: #analiza que sea true
    print("anda")

print("continua") 

mi_condicion = 5*3
if mi_condicion == 9: #analiza que sea true
    print("resultado coincide con 9")

if mi_condicion == 15: #analiza que sea true
    print("resultado coincide con 15")

if mi_condicion >= 9: #analiza que sea true
    print("resultado es mayor a 9")

if mi_condicion == 9: #analiza que sea true
    print("resultado es igual a 9")
else:
    print("es diferente de 9")

#varias consultas con and y or

if mi_condicion == 15 and mi_condicion <=10 : #analiza que sea true
    print("resultado es positivo")
else:
    print("es diferente")