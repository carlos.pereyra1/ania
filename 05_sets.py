### Sets ###

"""
Es parecido a las tuplas y listas, pero se inicia con set()
"""

mi_set = set()
#o tambien
#mi_set = {}
mi_otro_set = {}

print(type(mi_set)) #Imprime tipo de dato set
mi_otro_set = {"Carlin", 35, 1.70}

print(type(mi_otro_set)) #Imprime tipo de dato set
print (mi_otro_set) #un set no es una estructura ordenada

mi_otro_set.add("es informático")
print (mi_otro_set) #un set no es una estructura ordenada, es decir, no hace listado.

mi_otro_set.add("es informático")
print (mi_otro_set) #un set no admite repetidos, no se asegura ningun orden de datos.
 
mi_otro_set.remove("Carlin")
print(mi_otro_set) #remueve el dato seleccionado

mi_otro_set.clear()#borra todos los datos que existen en el set
print(len(mi_otro_set))

del mi_otro_set #borra la variable del sistema
#print(mi_otro_set) #no esta definido

mi_set = {"Carlin", 35, 1.70}

mi_lista = list(mi_set)
print(mi_lista)
print(mi_lista[0])#imprime el elemento 0 de la lista, es riesgoso porq el set no mantiene un orden

mi_otro_set = {"Pere", "Carlin", "Charly", 29}
#Para unir dos set

sumando_set = mi_set.union(mi_otro_set)
print(sumando_set) #en la suma tambien se refleja que no admite valores repetidos

print(sumando_set.union({"Pereyra", "Emiliano"}))#momentaneamente agrega esos datos con el Union, luego no los guarda

print(sumando_set)

#el union se puede utilizar cuantas veces se necesite.

print(mi_otro_set.difference(mi_set))#muestra los datos que son diferentes entre ambos